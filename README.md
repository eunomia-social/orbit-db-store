Original orbit-db.store README [here](./orig-README.md).

# Index snapshot support for OrbitDB - orbit-db-store module

## Installation
- Clone repository
- Install [node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) (in case you haven't already)
- Run `npm install`
- Run `npm test` to make sure everything is working
